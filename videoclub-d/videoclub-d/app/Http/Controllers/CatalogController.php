<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie;

class CatalogController extends Controller
{

	public function getIndex()
	{
		$movies = Movie::all();
 		return view('catalog.index', array('arrayPeliculas' => $movies));
	}


    public function getShow($id)
	{
		$movies = Movie::findOrFail($id);
	 	return view('catalog.show', array('pelicula' => $movies, 'id' => $id));
	}


	public function getCreate()
	{
	 	return view('catalog.create');
	}


	public function postCreate(Request $request)
	{	
		$pelicula = new Movie;
		$pelicula-> title  = $request->input('title');
		$pelicula-> year  = $request->input('year');
		$pelicula->director  = $request->input('director');
		$pelicula->poster  = $request->input('poster');
		$pelicula->rented  = false;
		$pelicula->synopsis  = $request->input('synopsis'); 
		$pelicula->save();

	 	return redirect('catalog');
	}


	public function getEdit($id)
	{
		$movies = Movie::findOrFail($id);
	 	return view('catalog.edit', array('pelicula'=> $movies, 'id' => $id));
	}	


	public function putEdit($id, Request $request)
	{
		$pelicula = Movie::find($id);
		$pelicula-> title  = $request->input('title');
		$pelicula-> year  = $request->input('year');
		$pelicula->director  = $request->input('director');
		$pelicula->poster  = $request->input('poster');
		$pelicula->synopsis  = $request->input('synopsis'); 
		$pelicula->save();
		

	 	return redirect('catalog');
	}	

}
