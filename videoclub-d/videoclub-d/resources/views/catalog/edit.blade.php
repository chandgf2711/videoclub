<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Modificar película</title>

  </head>
  <body>

    @extends('layouts.master')

    @section('content')

     <div class="panel-body">

       <form class="form-horizontal" role="form" method="POST" action="{{ route('catalog.edit', $id) }}">

          {{ csrf_field() }}
          {{ method_field('PUT') }}

          <div class="form-group">   
            <label for="title" class="col-md-4 control-label">Titulo:</label>
            <div class="col-md-6">
              <input id="title" type="text" class="form-control" name="title" value="{{ $pelicula->title }}" required autofocus>
            </div>
          </div>

          <div class="form-group">
            <label for="year" class="col-md-4 control-label">Año:</label>
            <div class="col-md-6">
              <input id="year" type="number" class="form-control" name="year" value="{{ $pelicula->year }}" required>
            </div>
          </div>

          <div class="form-group">
            <label for="director" class="col-md-4 control-label">Director:</label>
            <div class="col-md-6">
              <input id="director" type="text" class="form-control" name="director" value="{{ $pelicula->director }}" required>
            </div>
          </div>

          <div class="form-group">
            <label for="poster" class="col-md-4 control-label">Poster:</label>
            <div class="col-md-6">
              <input id="poster" type="text" class="form-control" name="poster" value="{{ $pelicula->poster }}" required>
            </div>
          </div>

          <div class="form-group">
            <label for="synopsis" class="col-md-4 control-label">Resumen:</label>
            <div class="col-md-6">
              <textarea  id="synopsis" type="textarea" class="form-control"  rows="10" name="synopsis" value="{{ $pelicula->synopsis }}" required></textarea>
            </div>
          </div>

          <div class="form-group">
            <div>
              <input type="submit" name="grabar" value="Modificar película" class="btn btn-danger">
            </div>
          </div>

      </form>

     </div>

    @stop

  </body>
</html>