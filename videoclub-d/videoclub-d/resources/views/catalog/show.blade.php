<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Vista detalle película</title>

  </head>
  <body>

    @extends('layouts.master')

    @section('content')

     <div class="row">

       <div class="col-sm-4">
        <img src="{{$pelicula->poster}}" style="height:300px"/>
       </div>

       <div class="col-sm-8">
        <h1>{{$pelicula->title}}</h1>
        <h2 class="h4">Año: {{$pelicula->year}}</h2>
        <h2 class="h4">Director: {{$pelicula->director}}</h2><br/>

        <p><strong>Resumen:</strong> {{$pelicula->synopsis}}</p><br/>
        <p><strong>Estado:</strong>
          @if( $pelicula->rented )
            Película actualmente alquilada<br/><br/>
            <a href="#" class="btn btn-danger">Devolver película</a>
          @else
            Película disponible<br/><br/>
            <a href="#" class="btn btn-danger">Alquilar película</a>
          @endif
          <a href="{{ url('/catalog/edit/' . $pelicula->id ) }}" class="btn btn-warning"><span class="glyphicon glyphicon-pencil"></span> Editar película</a>
          <a href="{{ url('catalog') }}" class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span>  Volver al listado</a>
        </p><br/>


       </div>

     </div>

    @stop

  </body>
</html>